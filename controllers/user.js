const { response } = 'express';

const usuarioGet = (req, res = response) => {
  res.json({
    msg: 'get API - controlador'
  });
};

const usuarioPost = (req, res) => {

  //req.body trae la informacion del body
  const {nombre, edad} = req.body;

  res.status(201).json({
    msg: 'post API',
    nombre,
    edad
  });
}


router.put('/', (req, res) => {
  res.json({
    msg: 'put API'
  });
});

router.post('/', (req, res) => {
  res.status(201).json({
    msg: 'post API'
  });
});

router.delete('/', (req, res) => {
  res.json({
    msg: 'delete API'
  });
});

module.exports = {
  usuarioGet,
  usuarioPost,
  usuarioPut,
  usuarioDelete
};
